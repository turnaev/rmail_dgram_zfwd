#cython: embedsignature=True

from libc.stdint cimport *
from libc.string cimport *
from libc.stdlib cimport *

import socket
import syslog

from cpython.string cimport PyString_Check, PyString_AS_STRING, PyString_GET_SIZE

cdef extern from "Python.h":
    ctypedef struct PyObject
    PyObject *PyExc_Exception
    PyObject *PyErr_SetFromErrno(PyObject *)

cdef extern from "zmq.h" nogil:

    void _zmq_version "zmq_version"(int *major, int *minor, int *patch)

    ctypedef int fd_t "ZMQ_FD_T"

    enum: ZMQ_VERSION_MAJOR
    enum: ZMQ_VERSION_MINOR
    enum: ZMQ_VERSION_PATCH
    enum: ZMQ_VERSION

    enum: ZMQ_HAUSNUMERO
    enum: ZMQ_ENOTSUP "ENOTSUP"
    enum: ZMQ_EPROTONOSUPPORT "EPROTONOSUPPORT"
    enum: ZMQ_ENOBUFS "ENOBUFS"
    enum: ZMQ_ENETDOWN "ENETDOWN"
    enum: ZMQ_EADDRINUSE "EADDRINUSE"
    enum: ZMQ_EADDRNOTAVAIL "EADDRNOTAVAIL"
    enum: ZMQ_ECONNREFUSED "ECONNREFUSED"
    enum: ZMQ_EINPROGRESS "EINPROGRESS"
    enum: ZMQ_ENOTSOCK "ENOTSOCK"
    enum: ZMQ_EFSM "EFSM"
    enum: ZMQ_ENOCOMPATPROTO "ENOCOMPATPROTO"
    enum: ZMQ_ETERM "ETERM"
    enum: ZMQ_ECANTROUTE "ECANTROUTE"
    enum: ZMQ_EMTHREAD "EMTHREAD"

    enum: errno
    char *zmq_strerror (int errnum)
    int zmq_errno()

    enum: ZMQ_MAX_VSM_SIZE # 30
    enum: ZMQ_DELIMITER # 31
    enum: ZMQ_VSM # 32
    enum: ZMQ_MSG_MORE # 1
    enum: ZMQ_MSG_SHARED # 128

    # blackbox def for zmq_msg_t
    ctypedef void * zmq_msg_t "zmq_msg_t"

    ctypedef void zmq_free_fn(void *data, void *hint)

    int zmq_msg_init (zmq_msg_t *msg)
    int zmq_msg_init_size (zmq_msg_t *msg, size_t size)
    int zmq_msg_init_data (zmq_msg_t *msg, void *data,
        size_t size, zmq_free_fn *ffn, void *hint)
    int zmq_msg_close (zmq_msg_t *msg)
    int zmq_msg_move (zmq_msg_t *dest, zmq_msg_t *src)
    int zmq_msg_copy (zmq_msg_t *dest, zmq_msg_t *src)
    void *zmq_msg_data (zmq_msg_t *msg)
    size_t zmq_msg_size (zmq_msg_t *msg)

    void *zmq_init (int io_threads)
    int zmq_term (void *context)

    enum: ZMQ_PAIR # 0
    enum: ZMQ_PUB # 1
    enum: ZMQ_SUB # 2
    enum: ZMQ_REQ # 3
    enum: ZMQ_REP # 4
    enum: ZMQ_XREQ # 5
    enum: ZMQ_DEALER # 5 or 12
    enum: ZMQ_XREP # 6
    enum: ZMQ_ROUTER # 6 or 11 or 13
    enum: ZMQ_PULL # 7
    enum: ZMQ_PUSH # 8
    enum: ZMQ_XPUB # 9
    enum: ZMQ_XSUB # 10
    enum: ZMQ_UPSTREAM # 7
    enum: ZMQ_DOWNSTREAM # 8

    enum: ZMQ_HWM # 1
    enum: ZMQ_SWAP # 3
    enum: ZMQ_AFFINITY # 4
    enum: ZMQ_IDENTITY # 5
    enum: ZMQ_SUBSCRIBE # 6
    enum: ZMQ_UNSUBSCRIBE # 7
    enum: ZMQ_RATE # 8
    enum: ZMQ_RECOVERY_IVL # 9
    enum: ZMQ_MCAST_LOOP # 10
    enum: ZMQ_SNDBUF # 11
    enum: ZMQ_RCVBUF # 12
    enum: ZMQ_RCVMORE # 13
    enum: ZMQ_FD # 14
    enum: ZMQ_EVENTS # 15
    enum: ZMQ_TYPE # 16
    enum: ZMQ_LINGER # 17
    enum: ZMQ_RECONNECT_IVL # 18
    enum: ZMQ_BACKLOG # 19
    enum: ZMQ_RECOVERY_IVL_MSEC # 20
    enum: ZMQ_RECONNECT_IVL_MAX # 21
    enum: ZMQ_MAXMSGSIZE # 22
    enum: ZMQ_SNDHWM # 23
    enum: ZMQ_RCVHWM # 24
    enum: ZMQ_MULTICAST_HOPS # 25
    enum: ZMQ_RCVTIMEO # 27
    enum: ZMQ_SNDTIMEO # 28
    enum: ZMQ_RCVLABEL # 29
    enum: ZMQ_RCVCMD # 30

    enum: ZMQ_NOBLOCK # 1
    enum: ZMQ_DONTWAIT # 1
    enum: ZMQ_SNDMORE # 2
    enum: ZMQ_SNDLABEL # 4
    enum: ZMQ_SNDCMD # 8

    void *zmq_socket (void *context, int type)
    int zmq_close (void *s)
    int zmq_setsockopt (void *s, int option, void *optval, size_t optvallen)
    int zmq_getsockopt (void *s, int option, void *optval, size_t *optvallen)
    int zmq_bind (void *s, char *addr)
    int zmq_connect (void *s, char *addr)
    # send/recv
    int zmq_sendmsg (void *s, zmq_msg_t *msg, int flags)
    int zmq_recvmsg (void *s, zmq_msg_t *msg, int flags)
    #int zmq_sendbuf (void *s, const_void_ptr buf, size_t n, int flags)
    #int zmq_recvbuf (void *s, void *buf, size_t n, int flags)


cdef extern from "ev.h":
    struct ev_watcher:
        void *data

    struct ev_io:
        void *data
        int fd
        int events

    struct ev_timer:
        void *data

    struct ev_signal:
        void *data
        int signum

    struct ev_loop

    ev_loop* ev_default_loop(unsigned int)
    void ev_default_destroy()
    void ev_io_stop(ev_loop *, ev_io *)

    ctypedef void CB_TYPE(ev_loop *loop, ev_io *w, int revents)

    void ev_io_init(ev_io *, CB_TYPE, int fd, unsigned int)
    void ev_io_start(ev_loop *,ev_io *)

    enum: EV_READ
    enum: EV_WRITE
    enum: EV_IO


cdef extern from "syslog.h":
    enum: LOG_EMERG
    enum: LOG_ALERT
    enum: LOG_CRIT
    enum: LOG_ERR
    enum: LOG_WARNING
    enum: LOG_NOTICE
    enum: LOG_INFO
    enum: LOG_DEBUG

    enum: LOG_USER


cdef extern from "cxsyslog.h":
    struct xlog_t:
        int sock_fd
        int fallback_fd

    struct log_conf_t:
        char tag[64]
        int facility_level[LOG_DEBUG + 1]

    xlog_t * xopenlog(char * addr, int err_stream_fd, log_conf_t * conf)
    int xloglog(xlog_t * log, char * msg, int level)
    #void xlogclose(xlog_t * log)
    #int xloglog(xlog_t * log, char * msg, unsigned int facility_priority, char * tag)


cdef extern from "dgram_fwd.h":
    struct forward_pair_t:
        int read_fd
        void * write_sock

    void io_cb(ev_loop *loop, ev_io *w, int revents)
    void loop_run(ev_loop *loop, log_conf_t * log)



cdef class UdpInput:
    cdef:
        object sock
        int sock_fd
        ev_loop * loop
        ev_io * w_recv

    def __dealloc__(self):
        if self.sock_fd > 0:
            self.sock.close()
            self.sock_fd = -1
            ev_io_stop(self.loop, self.w_recv)


cdef int udp_input_factory(UdpInput self, ev_loop * loop, udp_input, zmq_outputs) except -1:
    cdef:
        ZmqOutput z_output
        forward_pair_t * fwd_pair

    self.loop = loop

    self.sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    self.sock.setblocking(0)
    addr, port = udp_input['listen'].split(":")
    port = int(port)
    self.sock.bind((addr, port))

    z_output = zmq_outputs[udp_input['output']]

    fwd_pair = <forward_pair_t*>calloc(1, sizeof(forward_pair_t))
    if fwd_pair == NULL:
        PyErr_SetFromErrno(PyExc_Exception)
        return -1
    fwd_pair.read_fd = self.sock.fileno()
    fwd_pair.write_sock = z_output.zmq_sock

    self.w_recv = <ev_io *>calloc(1, sizeof(ev_io))
    if self.w_recv == NULL:
        free(fwd_pair)
        PyErr_SetFromErrno(PyExc_Exception)
        return -1

    self.w_recv.data = fwd_pair
    ev_io_init(self.w_recv, io_cb, fwd_pair.read_fd, EV_READ)
    ev_io_start(loop, self.w_recv)
    return 0




cdef class ZmqOutput:
    cdef:
        void * zmq_sock

    def __dealloc__(self):
        if self.zmq_sock:
            zmq_close(self.zmq_sock)
            self.zmq_sock = NULL

cdef int z_output_factory(ZmqOutput self, void * zmq_ctx, output_conf) except -1:
    cdef:
        uint64_t hwm_tmp

    self.zmq_sock = zmq_socket(zmq_ctx, ZMQ_PUSH)
    if not self.zmq_sock:
        PyErr_SetFromErrno(PyExc_Exception)
        return -1

    hwm_tmp = output_conf.get('hwm', 1024)
    if zmq_setsockopt(self.zmq_sock, ZMQ_HWM, &hwm_tmp, sizeof(uint64_t)) == -1:
        PyErr_SetFromErrno(PyExc_Exception)
        zmq_close(self.zmq_sock)
        return -1

    try:
        for connect_addr in output_conf['connect']:
            c_tmp = connect_addr
            if zmq_connect(self.zmq_sock, c_tmp) != 0:
                raise ValueError("zmq_connect error for addr: %s in zmq_output: %s" % (repr(connect_addr), repr(output_conf)))
    except:
        zmq_close(self.zmq_sock)
        return -1
    return 0


cdef int get_log_level(level_conf, name):
    cdef:
        int ret
    if name in level_conf:
        ll = level_conf.get(name, [])
        ret = 0
        for elm in ll:
            ret |= getattr(syslog, elm)
        #print "get_log_level returns: %d" % ret
        return ret
    #print "get_log_level returns: %d" % 0
    return 0


cdef class ForwardDaemon:
    cdef:
        void * zmq_ctx
        ev_loop * loop

        log_conf_t * log_conf

        object udp_inputs
        object zmq_outputs

    def __cinit__(self, conf):
        self.log_conf = <log_conf_t *>calloc(1, sizeof(log_conf_t))
        if 'xsyslog_conf' in conf:
            tag = conf['xsyslog_conf'].get('tag', 'dgram_zfwd')
            memcpy(self.log_conf.tag, PyString_AS_STRING(tag), min(PyString_GET_SIZE(tag), 63))

            level_conf = conf['xsyslog_conf'].get('level_conf', {})

            self.log_conf.facility_level[LOG_EMERG] = get_log_level(level_conf, "emerg")
            self.log_conf.facility_level[LOG_ALERT] = get_log_level(level_conf, "alert")
            self.log_conf.facility_level[LOG_CRIT] = get_log_level(level_conf, "critical")
            self.log_conf.facility_level[LOG_ERR] = get_log_level(level_conf, "error")
            self.log_conf.facility_level[LOG_WARNING] = get_log_level(level_conf, "warning")
            self.log_conf.facility_level[LOG_NOTICE] = get_log_level(level_conf, "notice")
            self.log_conf.facility_level[LOG_INFO] = get_log_level(level_conf, "info")
            self.log_conf.facility_level[LOG_DEBUG] = get_log_level(level_conf, "debug")

        forward_daemon_init(self, conf)

    def __dealloc__(self):
        if self.log_conf != NULL:
            free(self.log_conf)

    def run(self):
        loop_run(self.loop, self.log_conf)


cdef int forward_daemon_init(ForwardDaemon self, conf) except -1:

    self.loop = ev_default_loop(0)
    if self.loop == NULL:
        PyErr_SetFromErrno(PyExc_Exception)
        return -1

    self.udp_inputs = []
    self.zmq_outputs = {}

    self.zmq_ctx = zmq_init(1)
    if self.zmq_ctx == NULL:
        PyErr_SetFromErrno(PyExc_Exception)
        return -1

    for output_name, output_conf in conf['zmq_output'].items():
        z_output = ZmqOutput()
        z_output_factory(z_output, self.zmq_ctx, output_conf)
        self.zmq_outputs[output_name] = z_output


    for conf_udp_input in conf['udp_input']:
        udp_input = UdpInput()
        udp_input_factory(udp_input, self.loop, conf_udp_input, self.zmq_outputs)
        self.udp_inputs.append(udp_input)






