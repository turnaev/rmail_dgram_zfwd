

#include "dgram_fwd.h"

/*
 * global variables
 */
msgpack_sbuffer * g_buffer;
msgpack_packer * g_pk;
struct utsname * g_uname;
int g_uname_nodename_len;

xlog_t * xlog;

uint64_t rcv_cnt;
uint64_t send_cnt;


void sigint_cb (struct ev_loop *loop, ev_signal *w, int revents){
    ev_unloop (loop, EVUNLOOP_ALL);
}


void loop_run(struct ev_loop *loop, log_conf_t * log_conf){

    xlog = xopenlog("/dev/log", -1, log_conf);
    if(!xlog){
        fprintf(stderr, "Can't open log\n");
        exit(1);
    }

    // allocate msgpack buffers
    g_buffer = msgpack_sbuffer_new();
    g_pk = msgpack_packer_new(g_buffer, msgpack_sbuffer_write);

    // uname()
    g_uname = calloc(1, sizeof(struct utsname));
    if(! g_uname){
        xvlog(xlog, LOG_ERR, "Can't allocate struct utsname, %s", strerror(errno));
        exit(1);
    }
    if(uname(g_uname) != 0){
        xvlog(xlog, LOG_ERR, "uname() call error, %s", strerror(errno));
        exit(1);
    }
    g_uname_nodename_len = strlen(g_uname->nodename);

    // setup sigint exit cb
    ev_signal exitsig;
    ev_signal_init (&exitsig, sigint_cb, SIGINT);
    ev_signal_start (loop, &exitsig);

    xvlog(xlog, LOG_INFO, "starting loop.");
    ev_loop(loop, 0);
}


void io_cb(struct ev_loop *loop, ev_io *w, int revents){
    char buf[MAX_MSG];
    ssize_t read_size;
    int rc;
    zmq_msg_t msg;

    // time stuff
    struct tm cur_tm;
    char date_buf[STRLEN("2012-01-01") + 1];
    time_t t_time;
    int i_tmp;

    forward_pair_t * fwd_pair = w->data;

    // format current time in a form YYYY-MM-DD
    ev_tstamp ev_cur_time = ev_now(loop);
    t_time = (time_t) ev_cur_time;
    if(localtime_r( &t_time, &cur_tm) == NULL){
        xvlog(xlog, LOG_ERR, "localtime_r() call error, %s", strerror(errno));
        return;
    }

    i_tmp  = strftime(date_buf, STRLEN("2012-01-01") + 1, "%Y-%m-%d", &cur_tm);
    if(i_tmp <= 0){
        xvlog(xlog, LOG_ERR, "strftime() call error, %s", strerror(errno));
        return;
    }

    while(1){
        read_size = recvfrom(fwd_pair->read_fd, buf, MAX_MSG, MSG_DONTWAIT, 0, 0);
        if(read_size > 0){

            rcv_cnt++;

            xvlog(xlog, LOG_DEBUG, "rcv: %llu, send: %llu, read dgram: '%.*s'", rcv_cnt, send_cnt, (int)read_size, buf);

            msgpack_sbuffer_clear(g_buffer);
            // list
            msgpack_pack_array(g_pk, 4);
                // proto version
                msgpack_pack_int(g_pk, ZPROTO_VERSION);

                // our name
                msgpack_pack_raw(g_pk, g_uname_nodename_len);
                msgpack_pack_raw_body(g_pk, g_uname->nodename, g_uname_nodename_len);

                // message generation time
                msgpack_pack_unsigned_int(g_pk, (unsigned int) ev_cur_time);

                // array of dicts
                msgpack_pack_array(g_pk, 1);
                    // dict
                    msgpack_pack_map(g_pk, 2);

                    // key 1
                    msgpack_pack_raw(g_pk, STRLEN("json_struct"));
                    msgpack_pack_raw_body(g_pk, "json_struct", STRLEN("json_struct"));

                    // value 1
                    msgpack_pack_raw(g_pk, read_size);
                    msgpack_pack_raw_body(g_pk, buf, read_size);

                    // key 2
                    msgpack_pack_raw(g_pk, STRLEN("date"));
                    msgpack_pack_raw_body(g_pk, "date", STRLEN("date"));

                    // value 2
                    // value2: current date string representation
                    msgpack_pack_raw(g_pk, STRLEN("2012-01-01"));
                    msgpack_pack_raw_body(g_pk, date_buf, STRLEN("2012-01-01"));

            // send zmq msg
            rc = zmq_msg_init_size (&msg, g_buffer->size);
            if(rc == 0){
                memcpy (zmq_msg_data (&msg), g_buffer->data, g_buffer->size);
                rc = zmq_send(fwd_pair->write_sock, &msg, ZMQ_NOBLOCK);
                if(rc != 0){
                    xvlog(xlog, LOG_ERR, "zmq_send() error: %s", strerror(errno));
                }
                else{
                    send_cnt++;
                }
            }
            else{
                xvlog(xlog, LOG_ERR, "zmq_msg_init_size() error: %s", strerror(errno));
            }
            // free message data
            zmq_msg_close(&msg);
        }
        else{
            if (errno != EAGAIN){
                xvlog(xlog, LOG_ERR, "recvfrom() error: %s", strerror(errno));
            }
            break;
        }
    } // end while 1
}


