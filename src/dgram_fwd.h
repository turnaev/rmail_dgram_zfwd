
#include <stdint.h>

#include <ev.h>

#include <sys/types.h>
#include <sys/socket.h>

#include <stdint.h>
#include <string.h>

#include <time.h>
#include <sys/utsname.h>

#include <errno.h>

#define MAX_MSG 8192
#define ZPROTO_VERSION 3

#define STRLEN(s) (sizeof(s) - 1)

#include <msgpack.h>

#include <cxsyslog.h>
#include <syslog.h>

#include <zmq.h>

#ifndef DGRAM_ZFWD_H
#define DGRAM_ZFWD_H

typedef struct forward_pair_t {
    int read_fd;
    void * write_sock;
} forward_pair_t;

void loop_run(struct ev_loop *loop, log_conf_t * log_conf);
void io_cb(struct ev_loop *loop, ev_io *w, int revents);


#endif

