#!/usr/bin/env python

import socket
import time

ip = "127.0.0.1"
port = 59000

sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

i = 0
while 1:
    sock.sendto("Hello world %d" % i, (ip, port))
    i += 1
    time.sleep(0.01)
